import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})

export class NavigationComponent implements OnInit {

  route: string;
  showNavigation: boolean = false;

  constructor (location: Location, router: Router) {
    router.events.subscribe((val) => {
      const currentPath = location.path();
      this.showNavigation = currentPath === '/home' ? false : true;
    });
  }

  ngOnInit () { }
}
