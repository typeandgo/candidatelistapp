import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()

export class CandidatesService {

  candidateList: any[] = [];

  constructor (private http: HttpClient) { }

  getCandidates () {
    return this.http.get('https://randomuser.me/api?results=10&seed=4bd32f83f69cc82a')
      .map((response: any) => {
        this.candidateList = response.results;
        return response.results;
      });
  }

  getCandidateById (candidateId: number) {
    return this.candidateList[candidateId] || null;
  }
}
