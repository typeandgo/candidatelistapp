import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CandidatesService } from '../candidates.service';

@Component({
  selector: 'app-candidate-detail',
  templateUrl: './candidate-detail.component.html',
  styleUrls: ['./candidate-detail.component.scss']
})

export class CandidateDetailComponent implements OnInit {

  candidateId: number;
  candidate: object;

  constructor (private route: ActivatedRoute, private router: Router, private candidatesService: CandidatesService) { }

  ngOnInit () {
    this.candidateId = this.route.snapshot.params['id'];
    this.candidate = this.candidatesService.getCandidateById(this.candidateId);

    if (this.candidate === null) {
      this.router.navigate(['./candidates']);
    }
  }
}
