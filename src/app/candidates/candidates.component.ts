import { Component, OnInit } from '@angular/core';
import { CandidatesService } from './candidates.service';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.scss']
})

export class CandidatesComponent implements OnInit {

  candidateList: object;

  constructor (private candidatesService: CandidatesService) { }

  ngOnInit () {
    this.candidatesService.getCandidates()
      .subscribe(
        (response) =>  this.candidateList = response,
        (error) => console.log('error: ', error)
      );
  }
}
